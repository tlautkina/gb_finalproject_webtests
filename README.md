Задание 4. Написание автотестов на авторизацию.
ТЗ на раздел “Запрос ленты с постами” и описание функционала тестируемого проекта:
https://docs.google.com/document/d/19IDnFaUDtxTRbvBo-EN5maXb2bkNlmYEBmtJb8A58aE/edit?usp=sharing

Часть 1. Необходимо написать автотесты на авторизацию (текущую реализацию авторизации, ссылка https://test-stand.gb.ru/login )

у вас должен быть один положительный тест, один негативный, а также тесты, которые будут проверять корнер кейсы (граничные значения), 
где количество корнер кейсов вы определяете самостоятельно,

учтите также кейсы, которые будут проверять, что вы действительно авторизовались и открылась страница с постами пользователя. 

Результат: написаны автотесты на авторизацию.

1. Check of successful Authorization.

2. Negative check: Authorization with invalid login and valid password.

3. Negative check: Authorization with invalid login and valid password.

4. Authorization with empty login and password fields.

5. Authorization with cyrillic symbols in login field.

6. Authorization with special symbols in login field.

7. Authorization with 2 symbols in login field.

8. Authorization with 3 symbols in login field.

9. Authorization with 20 symbols in login field.

10. Authorization with 21 symbols in login field.


Часть 2. Необходимо написать автотесты на страницу со своими публикациями (текущую реализацию страницы с постами, ссылка https://test-stand.gb.ru/?sort=createdAt&order=ASC ):

(ps. для проверки всех кейсов вам необходимо создать публикации, чтобы среди написанных тестов были тесты на переход между страницами Previous Page и Next Page).
учтите кейсы, что у пользователя может и не быть публикаций,
также переход между страницами (1ая, 2ая, и тд),
в выдаче есть сами посты (проверьте, что в посте имеется изображение, название и описание).

Результат: написаны автотесты на страницу со своими публицакиями.

11. Create and delete test post.

12. Quantity of posts on one page.

13. Check of switching to the next page.

14. Check of switching to the next page and return to previous page.

15. Check of sorting from new to old posts (DESC).

16. Check of sorting from old to new posts (ASC).

17. Check of switching to other users posts page.

18. Check that the first user's post has a picture, a title, a description.

19. Check that user doesn't have any posts.

Часть 3. Если у вас упали автотесты, проанализируйте почему и напишите причину падения: 

Упали автотесты №2-10 на проверку авторизации с невалидными параметрами, т.к. сообщения об ошибке не соответствуют заявленным в ТЗ, и также упал автотест №12 на проверку кол-ва постов на странице, т.к. кол-во по факту не соответствует ТЗ. После исправления этих багов тесты будут работать корректно.
