import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class AbstractPage {
    static WebDriver driver;
    static WebDriverWait wait;
    Actions actions;

    public AbstractPage(WebDriver driver) {
        AbstractPage.driver = driver;
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(10));
        actions = new Actions(driver);
        PageFactory.initElements(driver, this);
    }

    public static WebDriver getDriver() {
        return driver;
    }
}
