import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class LoginPage extends AbstractPage {

    static final String urlLoginPage = "https://test-stand.gb.ru/login";
    static By inputUsername = By.xpath("//*[@id='login']/div[1]//input");
    static By inputPassword = By.xpath("//*[@id='login']/div[2]//input");
    static By btnLogin = By.className("mdc-button__label");
    static By errorMessage = By.xpath("//*[@class='error-block svelte-uwkxn9']/p[1]");
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public static void logIn(String username, String password) {
        Actions actions = new Actions(getDriver());
        actions.click(getDriver().findElement(inputUsername))
                .sendKeys(username)
                .click(getDriver().findElement(inputPassword))
                .sendKeys(password)
                .click(getDriver().findElement(btnLogin))
                //.pause(Duration.ofSeconds(1))
                .build()
                .perform();
    }

}
