import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.xpath;

public class MyPostsPage extends AbstractPage {

    static final String urlNextPage = "https://test-stand.gb.ru/?page=2";
    static final String urlPrevPage = "https://test-stand.gb.ru/?page=1";
    static final String urlDESC = "https://test-stand.gb.ru/?sort=createdAt&order=DESC";
    static final String urlASC = "https://test-stand.gb.ru/?sort=createdAt&order=ASC";
    static final String urlNotMyPostsPage = "https://test-stand.gb.ru/?owner=notMe&sort=createdAt&order=ASC";
    static By userNameLink = By.xpath("//*[@id='app']//nav//li[3]/a");
    static By btnCreateNewPost = cssSelector("#create-btn");
    static By inputTitle = xpath("//*[@id='create-item']/div/div/div[1]/div//input");
    static By testPicture = xpath("//*[@id='app']/main/div/div[3]/div[1]/a[1]/img");
    static By testTitle = xpath("//*[@id='app']/main/div/div[3]/div[1]/a[1]/h2");
    static By testDescription = xpath("//*[@id='app']/main/div/div[3]/div[1]/a[1]/div");
    static By noPostsMessage = xpath("//*[@id='app']/main/div/div[3]/p");
    static By inputDescription = xpath("//*[@id='create-item']//div/div[2]/div//span/textarea");
    static By inputImage = xpath("//*[@id='create-item']//div/div[6]/div//label/input");
    static By btnSave = xpath("//*[@id='create-item']//div/div[7]//button");
    static By btnDeletePost = xpath("//*[@id=\"app\"]/main//div[1]/div/div[1]/div[1]/button[2]");
    static By cardPost = By.cssSelector(".post.svelte-127jg4t");
    static By btnNextPage = cssSelector("a[href='/?page=2']");
    static By btnPrevPage = cssSelector("a[href='/?page=1']");
    static By btnOrderDESC = xpath("//*[@id='app']//div/div[2]/div[2]/div[1]//i[2]");
    static By btnOrderASC = xpath("//*[@id=\"app\"]//div/div[2]/div[2]/div[1]//i[1]");
    static By btnNotMyPosts = By.xpath("//*[@id=\"app\"]//div/div[2]/div[2]/div[2]//span");
    public MyPostsPage(WebDriver driver) {
        super(driver);
    }

}
