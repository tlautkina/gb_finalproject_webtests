import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public abstract class AbstractTest {

    // User with 11 posts
    static String username = "tlautkina";
    static String password = "a635964972";

    // user with 0 posts
    static String username2 = "TestNoPosts";
    static String password2 = "7a1ad4468b";

    @BeforeAll
    static void setUp() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        options.setHeadless(true);
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        options.setImplicitWaitTimeout(Duration.ofSeconds(10));
        options.setPageLoadTimeout(Duration.ofSeconds(10));
        AbstractPage.driver = new ChromeDriver(options);
        AbstractPage.wait = new WebDriverWait(AbstractPage.driver, Duration.ofSeconds(10));
    }


    @AfterAll
    static void shutDown() {
        AbstractPage.driver.quit();
    }
}