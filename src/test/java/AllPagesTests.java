import io.qameta.allure.Feature;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AllPagesTests extends AbstractTest {

    @BeforeEach
    void openLoginPage() {
        AbstractPage.getDriver().get(LoginPage.urlLoginPage);
    }

    @DisplayName("1. Check of successful Authorization")
    @Feature("Authorization")
    @Test
    public void loginExistUser() {
        LoginPage.logIn(username, password);
        assertThat("Username should be " + username,
                AbstractPage.getDriver().findElement(MyPostsPage.userNameLink).getText(),
                is(equalTo("Hello, " + username)));
    }

    @DisplayName("2. Negative check: Authorization with invalid login and valid password")
    @Feature("Authorization")
    @Test
    public void loginInvalidUsernameValidPassword() {
        LoginPage.logIn("testlogin", password);
        assertThat("Should be a message about an invalid login and password",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Проверьте логин и пароль"))); // in fact message = "Invalid credentials."
    }

    @DisplayName("3. Negative check: Authorization with invalid login and valid password")
    @Feature("Authorization")
    @Test
    public void loginValidUsernameInvalidPassword() {
        LoginPage.logIn(username, "test1234");
        assertThat("Should be a message about an invalid login and password",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Проверьте логин и пароль"))); // in fact message = "Invalid credentials."
    }

    @DisplayName("4. Authorization with empty login and password fields.")
    @Feature("Authorization")
    @Test
    public void loginEmptyUsernameEmptyPassword() {
        LoginPage.logIn("", "");
        assertThat("Should be a message about empty fields",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Поле не может быть пустым"))); // in fact message = "Invalid credentials."
    }

    @DisplayName("5. Authorization with cyrillic symbols in login field")
    @Feature("Authorization")
    @Test
    public void loginUsernameCyrillicCharacters() {
        LoginPage.logIn("тест", password);
        assertThat("Should be a message about invalid characters",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Неправильный логин. Может состоять только из латинских букв и цифр, без спецсимволов")));
        // in fact message = "Invalid credentials."
    }

    @DisplayName("6. Authorization with special symbols in login field")
    @Feature("Authorization")
    @Test
    public void loginUsernameSpecialCharacters() {
        LoginPage.logIn("!test", password);
        assertThat("Should be a message about invalid characters",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Неправильный логин. Может состоять только из латинских букв и цифр, без спецсимволов")));
        // in fact message = "Invalid credentials."
    }

    @DisplayName("7. Authorization with 2 symbols in login field")
    @Feature("Authorization")
    @Test
    public void loginUsernameTwoCharacters() {
        LoginPage.logIn("tl", password);
        assertThat("Should be a message about an invalid login length",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Неправильный логин. Может быть не менее 3 и не более 20 символов")));
        // in fact message = "Invalid credentials."
    }

    @DisplayName("8. Authorization with 3 symbols in login field")
    @Feature("Authorization")
    @Test
    public void loginUsernameThreeCharacters() {
        LoginPage.logIn("tla", password);
        assertThat("Should be a message about an invalid login and password",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Проверьте логин и пароль")));
        // in fact message = "Invalid credentials."
    }

    @DisplayName("9. Authorization with 20 symbols in login field")
    @Feature("Authorization")
    @Test
    public void loginUsername20Characters() {
        LoginPage.logIn("zxcvbnmasdfghjklqwer", password);
        assertThat("Should be a message about an invalid login and password",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Проверьте логин и пароль")));
        // in fact message = "Invalid credentials."
    }

    @DisplayName("10. Authorization with 21 symbols in login field")
    @Feature("Authorization")
    @Test
    public void loginUsername21Characters() {
        LoginPage.logIn("asdfghjklzxcvbnmqwert", password);
        assertThat("Should be a message about an invalid login length",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Неправильный логин. Может быть не менее 3 и не более 20 символов")));
        // in fact message = "Invalid credentials."
    }


    @DisplayName("11. Create and delete test post")
    @Feature("Page 'My posts'")
    @Test
    void createAndDeletePost() {
        LoginPage.logIn(username, password);
        AbstractPage.getDriver().findElement(MyPostsPage.btnCreateNewPost).click();
        AbstractPage.getDriver().findElement(MyPostsPage.inputTitle).sendKeys("Post for delete");
        AbstractPage.getDriver().findElement(MyPostsPage.inputDescription).sendKeys("This is new post");
        AbstractPage.getDriver().findElement(MyPostsPage.inputImage).sendKeys("C:\Users\mrako\IdeaProjects\GB_FinalProject_WEBTests\src\main\resources\Cake.jpg");
        AbstractPage.getDriver().findElement(MyPostsPage.btnSave).click();
        AbstractPage.getDriver().findElement(MyPostsPage.btnDeletePost).click();
        //new Actions(getDriver()).pause(Duration.ofSeconds(1)).perform();
        new Actions(AbstractPage.getDriver()).pause(1000).perform();
        assertThrows(WebDriverException.class,
                () -> AbstractPage.getDriver().findElement(By.xpath("//h1[.='Post for delete']")));
    }

    @DisplayName("12. Quantity of posts on one page")
    @Feature("Page 'My posts'")
    @Test
    void countPostCards() {
        LoginPage.logIn(username, password);
        List<WebElement> cards = AbstractPage.getDriver().findElements(MyPostsPage.cardPost);
        Integer countCards = cards.size();

        //Use this code to make new test posts if test user doesn't have enough.
        /*
        if (countCards < 10) {
        for (int i = 0; i < 10; i++) {
        AbstractPage.getDriver().findElement(MyPostsPage.btnCreateNewPost).click();
        AbstractPage.getDriver().findElement(MyPostsPage.inputTitle).sendKeys("My Post");
        AbstractPage.getDriver().findElement(MyPostsPage.inputDescription).sendKeys("This is new post");
        AbstractPage.getDriver().findElement(MyPostsPage.inputImage).sendKeys("C:\Users\mrako\IdeaProjects\GB_FinalProject_WEBTests\src\main\resources\Cake.jpg");
        AbstractPage.getDriver().findElement(MyPostsPage.btnSave).click();
        new Actions(AbstractPage.getDriver()).pause(2000).perform();
        AbstractPage.getDriver().findElement(MyPostsPage.btnHomePage).click();
          }
        }
        */
        cards = AbstractPage.getDriver().findElements(MyPostsPage.cardPost);
        countCards = cards.size();
        assertEquals(10, countCards);
        //in fact there are only 4 posts on each page.
    }

    @DisplayName("13. Check of switching to the next page")
    @Feature("Page 'My posts'")
    @Test
    void goToTheNextPage() {
        LoginPage.logIn(username, password);
        AbstractPage.getDriver().findElement(MyPostsPage.btnNextPage).click();
        AbstractPage.wait.until(ExpectedConditions.urlToBe(MyPostsPage.urlNextPage));
        assertThat(AbstractPage.getDriver().getCurrentUrl(), is(equalTo(MyPostsPage.urlNextPage)));
    }

    @DisplayName("14. Check of switching to the next page and return to previous page")
    @Feature("Page 'My posts'")
    @Test
    void goToThePrevPage() {
        LoginPage.logIn(username, password);
        AbstractPage.getDriver().findElement(MyPostsPage.btnNextPage).click();
        AbstractPage.getDriver().findElement(MyPostsPage.btnPrevPage).click();
        AbstractPage.wait.until(ExpectedConditions.urlToBe(MyPostsPage.urlPrevPage));
        assertThat(AbstractPage.getDriver().getCurrentUrl(), is(equalTo(MyPostsPage.urlPrevPage)));
    }

    @DisplayName("15. Check of sorting from new to old posts (DESC)")
    @Feature("Page 'My posts'")
    @Test
    void sortingPostsByDateDESC() {
        LoginPage.logIn(username, password);
        AbstractPage.getDriver().findElement(MyPostsPage.btnOrderDESC).click();
        assertThat(AbstractPage.getDriver().getCurrentUrl(), is(equalTo(MyPostsPage.urlDESC)));
    }

    @DisplayName("16. Check of sorting from old to new posts (ASC)")
    @Feature("Page 'My posts'")
    @Test
    void sortingPostsByDateASC() {
        LoginPage.logIn(username, password);
        AbstractPage.getDriver().findElement(MyPostsPage.btnOrderDESC).click();
        AbstractPage.getDriver().findElement(MyPostsPage.btnOrderASC).click();
        assertThat(AbstractPage.getDriver().getCurrentUrl(), is(equalTo(MyPostsPage.urlASC)));
    }

    @DisplayName("17. Check of switching to other users posts page")
    @Feature("Page 'My posts'")
    @Test
    void openLoginNewPage() {
        LoginPage.logIn(username, password);
        AbstractPage.getDriver().findElement(MyPostsPage.btnNotMyPosts).click();
        assertThat(AbstractPage.getDriver().getCurrentUrl(), Matchers.is(Matchers.equalTo(MyPostsPage.urlNotMyPostsPage)));
    }

    @DisplayName("18. Check that the first user's post has a picture, a title, a description")
    @Feature("Page 'My posts'")
    @Test
    void checkTestPicture() {
        LoginPage.logIn(username, password);
        Assertions.assertTrue(AbstractPage.getDriver().findElement(MyPostsPage.testPicture).isDisplayed());
        assertThat(
                AbstractPage.getDriver().findElement(MyPostsPage.testTitle).getText(),
                is(equalTo("TestPost4")));
        assertThat(
                AbstractPage.getDriver().findElement(MyPostsPage.testDescription).getText(),
                is(equalTo("Test Post 4")));
    }

    @DisplayName("19. Check that user doesn't have any posts")
    @Feature("Page 'My posts'")
    @Test
    void checkEmptyPostsUser() {
        LoginPage.logIn(username2, password2);
        assertThat(
                AbstractPage.getDriver().findElement(MyPostsPage.noPostsMessage).getText(),
                is(equalTo("No items for your filter")));
    }

}
